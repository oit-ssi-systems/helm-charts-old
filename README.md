# SSI-Systems Helm Charts

Custom helm charts for the SSI-Systems team

## Usage

Add the repository to helm with

```
helm repo add ssi-systems https://ssiaks-harbor-default.oit.duke.edu/chartrepo/ssi-systems
```

You can search using something like

```
helm search repo ssi-systems/
```
